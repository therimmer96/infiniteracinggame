﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGateController : MonoBehaviour {

	private GameController controller;

	private void Start()
	{
		controller = GameObject.FindWithTag("GameController").GetComponent<GameController>();
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (controller.previousSegmentsList[0] == gameObject)
			{
				controller.NewSegment();
			}
			controller.NewSegment();
			GetComponent<BoxCollider>().enabled = false;

			controller.playerScore += 10;
		}

	}
}
