﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSystem : MonoBehaviour
{

	public GameObject missilePrefab;
	public Transform missilePoint;

	public MissileController loadedMissile = null;

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown("p"))
		{
			NewMissile();
		}

		if (Input.GetKeyDown("r"))
		{
			Fire();
		}
	}

	private void Fire()
	{
		loadedMissile.Fire();
		loadedMissile = null;
	}

	public void NewMissile()
	{
		if (loadedMissile == null)
		{
			loadedMissile = Instantiate(missilePrefab, missilePoint.position, missilePoint.rotation, missilePoint.parent).GetComponent<MissileController>();
		}
	}
}
