﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControlTemp : MonoBehaviour
{
	private Rigidbody rb;
	private GameController gc;

	#region Wheel Colliders
	[Header("Wheel Colliders")]
	[SerializeField]
	private WheelCollider colliderFL;
	[SerializeField]
	private WheelCollider colliderFR;
	[SerializeField]
	private WheelCollider colliderBL;
	[SerializeField]
	private WheelCollider colliderBR;
	#endregion

	#region Wheel Meshes
	[Header("Wheel Meshs")]
	[SerializeField]
	private Transform transformFL;
	[SerializeField]
	private Transform transformFR;
	[SerializeField]
	private Transform transformBL;
	[SerializeField]
	private Transform transformBR;
	#endregion

	#region Car Settings
	[Header("Car Settings")]
	[SerializeField]
	private bool frontWheelDrive = true;
	[SerializeField]
	private bool rearWheelDrive = true;

	[SerializeField]
	private bool frontWheelBrakes = false;
	[SerializeField]
	private bool RearWheelBrakes = true;

	[SerializeField]
	private bool frontWheelSteer = true;
	[SerializeField]
	private bool rearWheelSteer = false;

	public float topSpeedForward;
	public float topSpeedReverse;
	public float motorTorqueForward = 10000;
	public float motorTorqueReverse = 2500;
	public float maxSteerAngle = 50;
	public float brakingTorque = 10000;

	public bool hitByPlayer = false;

	public Transform centerOfMass;
	#endregion

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();

		rb.centerOfMass = centerOfMass.localPosition;

		#region Configure Substeps
		colliderFL.ConfigureVehicleSubsteps(5, 12, 15);
		colliderFR.ConfigureVehicleSubsteps(5, 12, 15);
		colliderBL.ConfigureVehicleSubsteps(5, 12, 15);
		colliderBR.ConfigureVehicleSubsteps(5, 12, 15);
		#endregion

		transform.parent = null;

		gc = GameObject.FindWithTag("GameController").GetComponent<GameController>();
	}

	// Update is called once per frame
	void Update()
	{
		colliderFL.motorTorque = motorTorqueForward;
		colliderFR.motorTorque = motorTorqueForward;

		colliderBL.motorTorque = motorTorqueForward;
		colliderBR.motorTorque = motorTorqueForward;

		WheelMatchVisual(colliderFL, transformFL);
		WheelMatchVisual(colliderFR, transformFR);
		WheelMatchVisual(colliderBL, transformBL);
		WheelMatchVisual(colliderBR, transformBR);

		bool onTrack = false;

		if (!colliderBL.isGrounded)
		{
			foreach (GameObject item in gc.previousSegmentsList)
			{
				if (transform.position.y > item.transform.position.y && !onTrack)
				{
					onTrack = true;
				}
			} 
		}

		if (!onTrack)
		{
			if (hitByPlayer)
			{
				gc.playerScore += 10;
			}
			DestroyObject(this);
		}
	}

	private void WheelMatchVisual(WheelCollider col, Transform mesh)
	{
		Vector3 position;
		Quaternion rotation;
		col.GetWorldPose(out position, out rotation);

		mesh.rotation = rotation;
		mesh.position = position;
	}
}
