﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PickupController : MonoBehaviour
{
	public enum PickupType{random,missile};

	public PickupType type;


	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			if (type == PickupType.random)
			{
				type = (PickupType)UnityEngine.Random.Range(1, Enum.GetNames(typeof(PickupType)).Length);
			}

			switch (type)
			{
				case PickupType.missile:
					other.GetComponent<MissileSystem>().NewMissile();
					break;
			}

			Destroy(gameObject, 0);	
		}
	}
}
