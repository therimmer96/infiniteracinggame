﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarController : MonoBehaviour
{
	public float maxSteerAngleMult;

	public float steerAngleDebug;
	public float steerAngleDebug2;

	private Rigidbody rb;

	#region Wheel Colliders
	[Header("Wheel Colliders")]
	[SerializeField]
	private WheelCollider colliderFL;
	[SerializeField]
	private WheelCollider colliderFR;
	[SerializeField]
	private WheelCollider colliderBL;
	[SerializeField]
	private WheelCollider colliderBR;
	#endregion

	#region Wheel Meshes
	[Header("Wheel Meshs")]
	[SerializeField]
	private Transform transformFL;
	[SerializeField]
	private Transform transformFR;
	[SerializeField]
	private Transform transformBL;
	[SerializeField]
	private Transform transformBR;
	#endregion

	#region Car Settings
	[Header("Car Settings")]
	[SerializeField]
	private bool frontWheelDrive = false;
	[SerializeField]
	private bool rearWheelDrive = true;

	[SerializeField]
	private bool frontWheelBrakes = false;
	[SerializeField]
	private bool RearWheelBrakes = true;

	[SerializeField]
	private bool frontWheelSteer = true;
	[SerializeField]
	private bool rearWheelSteer = false;

	public float topSpeedForward;
	public float topSpeedReverse;
	public float motorTorqueForward = 10000;
	public float motorTorqueReverse = 2500;
	public float maxSteerAngle = 50;
	public float brakingTorque = 10000;

	public float flightTorque = 10;

	public float boostFuelMax = 100;
	public float boostFuelCurrent;
	public float boostFuelCost = 50;
	public float boostFuelRegen = 50;
	public float boostForce = 10000;

	public Transform centerOfMass;
	#endregion

	#region ABS Booleans
	private bool frontABS;
	private bool rearABS;
	#endregion

	public List<WheelPair> wheelList;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();

		rb.centerOfMass = centerOfMass.localPosition;

		boostFuelCurrent = boostFuelMax;

		#region Configure Substeps
		colliderFL.ConfigureVehicleSubsteps(5, 12, 15);
		colliderFR.ConfigureVehicleSubsteps(5, 12, 15);
		colliderBL.ConfigureVehicleSubsteps(5, 12, 15);
		colliderBR.ConfigureVehicleSubsteps(5, 12, 15);
		#endregion
	}

	// Update is called once per frame
	void Update()
	{
		resetWheels();

		#region Throttle and brakes
		float leftTrigger = Input.GetAxis("LeftTrigger");
		float rightTrigger = Input.GetAxis("RightTrigger");

		Vector3 currentVelocity = transform.InverseTransformDirection(rb.velocity);

		float perWheelTorqueForward;

		if (frontWheelDrive && rearWheelDrive)
		{
			perWheelTorqueForward = motorTorqueForward / 4;
		}
		else
		{
			perWheelTorqueForward = motorTorqueForward / 2;
		}

		if (frontWheelDrive && currentVelocity.z * 2.23694 < topSpeedForward)
		{
			colliderFL.motorTorque = (perWheelTorqueForward * rightTrigger);
			colliderFR.motorTorque = (perWheelTorqueForward * rightTrigger);
		}
		if (rearWheelDrive && currentVelocity.z * 2.23694 < topSpeedForward)
		{
			colliderBL.motorTorque = (perWheelTorqueForward * rightTrigger);
			colliderBR.motorTorque = (perWheelTorqueForward * rightTrigger);
		}

		if (rb.velocity.magnitude > 1 && Vector3.Angle(transform.forward, rb.velocity) < 50f)
		{
			if (frontWheelBrakes && colliderFL.rpm > 5)
			{
				colliderFL.brakeTorque = (brakingTorque * leftTrigger);
				colliderFR.brakeTorque = (brakingTorque * leftTrigger);
			}
			if (RearWheelBrakes/* && colliderBL.rpm > 5*/)
			{
				colliderBL.brakeTorque = (brakingTorque * leftTrigger);
				colliderBR.brakeTorque = (brakingTorque * leftTrigger);
			}

			//frontABS = !frontABS;
			//rearABS = !rearABS;

			if (rb.velocity.magnitude < 10)
			{
				frontABS = true;
				rearABS = true;
			}
		}
		else if (leftTrigger > 0)
		{
			if (frontWheelDrive && currentVelocity.z * 2.23694 > -topSpeedReverse)
			{
				colliderFL.motorTorque = (-motorTorqueReverse * leftTrigger);
				colliderFR.motorTorque = (-motorTorqueReverse * leftTrigger);
			}
			if (rearWheelDrive && currentVelocity.z * 2.23694 > -topSpeedReverse)
			{
				colliderBL.motorTorque = (-motorTorqueReverse * leftTrigger);
				colliderBR.motorTorque = (-motorTorqueReverse * leftTrigger);
			}
		}
		#endregion

		#region Steering

		maxSteerAngleMult = (currentVelocity.z * 2.23694f) / topSpeedForward;
		steerAngleDebug2 = (currentVelocity.z * 2.23694f);
		steerAngleDebug = Mathf.Clamp(maxSteerAngle * maxSteerAngleMult, 20, maxSteerAngle);

		if (frontWheelSteer)
		{
			colliderFL.steerAngle = Input.GetAxis("Steer") * Mathf.Clamp(maxSteerAngle * maxSteerAngleMult, 20, maxSteerAngle);
			colliderFR.steerAngle = Input.GetAxis("Steer") * Mathf.Clamp(maxSteerAngle * maxSteerAngleMult, 20, maxSteerAngle);
		}
		if (rearWheelSteer)
		{
			colliderBL.steerAngle = Input.GetAxis("Steer") * -Mathf.Clamp(maxSteerAngle * maxSteerAngleMult, 20, maxSteerAngle);
			colliderBR.steerAngle = Input.GetAxis("Steer") * -Mathf.Clamp(maxSteerAngle * maxSteerAngleMult, 20, maxSteerAngle);
		}
		#endregion
		bool airborne = false;
		if (!colliderFL.isGrounded && !colliderFR.isGrounded && !colliderBL.isGrounded && !colliderBR.isGrounded)
		{
			airborne = true;
		}

		#region Flight
		if (airborne)
		{
			rb.AddRelativeTorque(flightTorque * (0 - Input.GetAxis("RightTrigger") + Input.GetAxis("LeftTrigger")),
				0,
				flightTorque * Input.GetAxis("Steer") * -1, ForceMode.Impulse);
		}
		#endregion

		#region Boost

		boostFuelCurrent = Mathf.Clamp(boostFuelCurrent + boostFuelRegen * Time.deltaTime, 0, boostFuelMax);

		if (Input.GetKeyDown("q") && boostFuelCurrent >= boostFuelCost && !airborne)
		{
			rb.AddRelativeForce(-boostForce, 0, 0 , ForceMode.Impulse);

			boostFuelCurrent = Mathf.Clamp(boostFuelCurrent - boostFuelCost, 0, 100);
		}

		if (Input.GetKeyDown("e") && boostFuelCurrent >= boostFuelCost && !airborne)
		{
			rb.AddRelativeForce(boostForce, 0, 0, ForceMode.Impulse);

			boostFuelCurrent = Mathf.Clamp(boostFuelCurrent - boostFuelCost, 0, 100);
		}

		if (Input.GetKeyDown("space") && boostFuelCurrent >=  boostFuelCost && !airborne)
		{
			rb.AddRelativeForce(0, boostForce, 0, ForceMode.Impulse);

			boostFuelCurrent = Mathf.Clamp(boostFuelCurrent - boostFuelCost, 0, 100);
		}

		if (Input.GetKeyDown("left shift") && boostFuelCurrent >= boostFuelCost)
		{
			rb.AddRelativeForce(0, 0, boostForce, ForceMode.Impulse);

			boostFuelCurrent = Mathf.Clamp(boostFuelCurrent - boostFuelCost, 0, 100);
		}

		if (Input.GetKeyDown(KeyCode.LeftControl) && boostFuelCurrent >= boostFuelCost)
		{
			rb.AddRelativeForce(0, 0, -boostForce, ForceMode.Impulse);

			boostFuelCurrent = Mathf.Clamp(boostFuelCurrent - boostFuelCost, 0, 100);
		}
		#endregion

		#region WheelMatchVisual
		WheelMatchVisual(colliderFL, transformFL);
		WheelMatchVisual(colliderFR, transformFR);
		WheelMatchVisual(colliderBL, transformBL);
		WheelMatchVisual(colliderBR, transformBR);
		#endregion
	}

	private void resetWheels()
	{
		colliderFL.brakeTorque = 0;
		colliderFR.brakeTorque = 0;
		colliderBL.brakeTorque = 0;
		colliderBR.brakeTorque = 0;

		colliderFL.motorTorque = 0;
		colliderFR.motorTorque = 0;
		colliderBL.motorTorque = 0;
		colliderBR.motorTorque = 0;
	}

	private void WheelMatchVisual(WheelCollider col, Transform mesh)
	{
		Vector3 position;
		Quaternion rotation;
		col.GetWorldPose(out position, out rotation);

		mesh.rotation = rotation;
		mesh.position = position;
	}
}

[Serializable]
public class WheelPair //for later use
{
	public bool drivePair;
	public bool steerPair;
	public bool brakePair;

	//this will be used to divide steering angle in order to allow cars with more than 2 sets of wheels to turn properly. Use negative numbers to invert the angle for "rear" wheels
	public float steeringMultiplyer = 1; 

	#region Wheel Colliders
	[Header("Wheel Colliders")]
	[SerializeField]
	private WheelCollider colliderLeft;
	[SerializeField]
	private WheelCollider colliderRight;

	#endregion

	#region Wheel Meshes
	[Header("Wheel Meshs")]
	[SerializeField]
	private Transform transformLeft;
	[SerializeField]
	private Transform transformRight;
	#endregion

	public void ApplyControl(float totalMotorTorque, float totalBrakeTorque, float steeringAngle)
	{
		if (drivePair)
		{
			colliderLeft.motorTorque = totalBrakeTorque / 2;
			colliderRight.motorTorque = totalBrakeTorque / 2;
		}

		if (steerPair)
		{
			colliderLeft.steerAngle = steeringAngle / steeringMultiplyer;
			colliderRight.steerAngle = steeringAngle / steeringMultiplyer;
		}

		if (brakePair)
		{
			colliderLeft.motorTorque = totalBrakeTorque / 2;
			colliderRight.motorTorque = totalBrakeTorque / 2;
		}

		#region Apply Visual
		WheelMatchVisual(colliderLeft, transformLeft);
		WheelMatchVisual(colliderRight, transformRight); 
		#endregion
	}

	private void WheelMatchVisual(WheelCollider col, Transform mesh)
	{
		Vector3 position;
		Quaternion rotation;
		col.GetWorldPose(out position, out rotation);

		mesh.rotation = rotation;
		mesh.position = position;
	}
}
