﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour
{
	private Rigidbody rb;

	private float radius = 25;
	private float power = 1000000;
	private bool notFired = true;

	public GameObject expPrefab;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	private void OnCollisionEnter(Collision collision)
	{
		Log.Add("BOOP");

		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
		foreach (Collider hit in colliders)
		{
			Rigidbody rb = hit.GetComponent<Rigidbody>();

			if (rb != null)
			{
				rb.AddExplosionForce(power, explosionPos, radius, 100.0F);
				Log.Add(hit.name);
				if (hit.tag == "Enemy")
				{
					EnemyControlTemp ec = hit.GetComponent<EnemyControlTemp>();
					ec.hitByPlayer = true;
				}
			}
		}
		Instantiate(expPrefab, transform.position, transform.rotation, null);
		Destroy(gameObject, 0);
	}

	//private void Update()
	//{
	//	if (Input.GetKeyDown("r") && notFired)
	//	{
	//		Fire();
	//	}
	//}

	public void Fire()
	{
		rb.isKinematic = false;
		rb.useGravity = true;
		notFired = true;
		name = name + " FIRED";
		transform.parent = null;
		rb.AddRelativeForce(Vector3.forward * 50, ForceMode.Impulse);
	}
}
