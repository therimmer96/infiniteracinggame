﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
	public GameObject[] roadPrefabsList;
	public GameObject lastSegment;
	public int maxSegments = 10;
	public int preloadCount;
	public int playerScore = 0;

	public List<GameObject> previousSegmentsList = new List<GameObject>();

	private void Start()
	{
		if (preloadCount >= maxSegments)
		{
			Log.AddError("Preload Count has to be atleast 1 under Max Segments");
		}
		if (preloadCount > 0)
		{
			for (int i = 0; i < preloadCount; i++)
			{
				NewSegment();
			}
		}

		playerScore = 0;
	}

	//private void Update()
	//{
	//	if (Input.GetKeyDown("space"))
	//	{
	//		NewSegment();
	//	}
	//}

	public void NewSegment()
	{
		if (lastSegment != null)
		{
			RoadSettings lastSegmentSettings = lastSegment.GetComponent<RoadSettings>();

			Vector3 spawnPoint = lastSegmentSettings.spawnPoint.transform.position;
			Quaternion spawnRotation = lastSegmentSettings.spawnPoint.transform.rotation;

			GameObject newSegment = Instantiate(roadPrefabsList[UnityEngine.Random.Range(0, roadPrefabsList.Length)]);

			RoadSettings newSegmentSettings = newSegment.GetComponent<RoadSettings>();

			newSegment.transform.position = spawnPoint;
			newSegment.transform.rotation = spawnRotation;

			lastSegment = newSegment;

			previousSegmentsList.Add(newSegment);
		}
		else
		{
			GameObject newSegment = Instantiate(roadPrefabsList[0]);

			lastSegment = newSegment;

			previousSegmentsList.Add(newSegment);
		}

		if (previousSegmentsList.Count > maxSegments)
		{
			DestroyObject(previousSegmentsList[0], 0);
			previousSegmentsList.RemoveAt(0);
		}
	}
}
